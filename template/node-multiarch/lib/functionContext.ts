/**
 * functionContext
 */

/* eslint-disable class-methods-use-this */

/* Node modules */

/* Third-party modules */

/* Files */
import { IFunctionContext, IKeyValue } from './interfaces';

export default class FunctionContext implements IFunctionContext {
  public httpHeaders: IKeyValue = {};

  public httpStatus: number = 200;

  /* The methods below exist for compatibility with the official node12 template */
  fail<T = any>(value: T): Promise<T> {
    return Promise.reject(value);
  }

  headers(): IKeyValue;
  headers(value: IKeyValue): IFunctionContext;
  headers(value?: IKeyValue): any {
    if (!value) {
      return this.httpHeaders;
    }

    this.httpHeaders = value;
    return this;
  }

  status(): number;
  status(value: number): IFunctionContext;
  status(value?: number): any {
    if (!value) {
      return this.httpStatus;
    }

    this.httpStatus = value;
    return this;
  }

  async succeed<T = any>(value: T): Promise<T> {
    return value;
  }
}
