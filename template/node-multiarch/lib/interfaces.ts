/**
 * interfaces
 */

/* Node modules */
import { IncomingHttpHeaders } from 'http';

/* Third-party modules */
import * as pino from 'pino';
import { ParsedQs } from 'qs';

/* Files */

export interface IKeyValue {
  [key: string]: any;
}

export interface IFunctionContext {
  httpHeaders: IKeyValue;
  httpStatus: number;
  fail(value: any): void;
  headers(): IKeyValue;
  headers(IKeyValue): IFunctionContext;
  status(): number;
  status(number): IFunctionContext;
  succeed(value: any): void;
}

export interface IFunctionEvent<ReqBody = any, ReqQuery = ParsedQs> {
  body: ReqBody;
  headers: IncomingHttpHeaders;
  method: string;
  query: ReqQuery;
  path: string;
  log: pino.Logger;
  getSecret(string): Promise<string>;
}

export interface IHandler {
  (event: IFunctionEvent, context: IFunctionContext): any | Promise<any>;
}
