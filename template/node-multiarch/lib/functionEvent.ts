/**
 * functionEvent
 */

/* Node modules */
import { promises as fs } from 'fs';
import { IncomingHttpHeaders } from 'http';

/* Third-party modules */
import * as express from 'express';
import * as pino from 'pino';
import * as uuid from 'uuid';

/* Files */
import { IFunctionEvent } from './interfaces';

export default class FunctionEvent implements IFunctionEvent {
  body: any;

  headers: IncomingHttpHeaders;

  method: string;

  query: any;

  path: string;

  log: pino.Logger;

  constructor(req: express.Request, logger: pino.Logger) {
    this.body = req.body;
    this.headers = req.headers;
    this.method = req.method;
    this.query = req.query;
    this.path = req.path;

    this.log = logger.child({ requestId: uuid.v4() });
  }

  /**
   * Get Secret
   *
   * Helper function to get secret value
   *
   * @param {string} secretName
   * @returns {Promise<string>}
   */
  // eslint-disable-next-line class-methods-use-this
  async getSecret(secretName: string): Promise<string> {
    try {
      return await fs.readFile(`/var/openfaas/secrets/${secretName}`, 'utf8');
    } catch {
      return fs.readFile(`/run/secrets/${secretName}`, 'utf8');
    }
  }
}
