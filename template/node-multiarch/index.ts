/**
 * index
 */

/* Node modules */

/* Third-party modules */
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as pino from 'pino';

/* Files */
import FunctionEvent from './lib/functionEvent';
import { IHandler } from './lib/interfaces';
import FunctionContext from './lib/functionContext';

const app = express();
app.disable('x-powered-by');

const logger = pino({
  level: process.env.LOGGER_LEVEL ?? 'info',
  redact: (process.env.LOGGER_REDACT ?? '')
    .split(',')
    .map((item) => item.trim())
    .filter((item) => item),
});

if (process.env.RAW_BODY === 'true') {
  app.use(bodyParser.raw({ type: '*/*' }));
} else {
  const jsonLimit = process.env.MAX_JSON_SIZE || '100kb'; // body-parser default
  app.use(bodyParser.json({ limit: jsonLimit }));
  app.use(bodyParser.raw()); // "Content-Type: application/octet-stream"
  app.use(bodyParser.text({ type: 'text/*' }));
}

const isArray = (input: any): boolean => !!input && input.constructor === Array;

const isObject = (input: any): boolean => !!input && input.constructor === Object;

const loadHandler = async (): Promise<IHandler> => {
  const jsFile = './function/handler';
  const tsFile = './function/tsHandler';

  let handler: any;
  try {
    /* Prioritise JS */
    // @ts-ignore
    handler = await import(jsFile);
    logger.debug({ jsFile }, 'Loading JavaScript file');
  } catch (err) {
    if (err.code !== 'MODULE_NOT_FOUND') {
      /* Script error - throw */
      throw err;
    }

    try {
      /* No JS file - try TS instead */
      // @ts-ignore
      handler = await import(tsFile);
      logger.debug({ tsFile }, 'Loading TypeScript file');
    } catch (tsErr) {
      if (tsErr.code === 'MODULE_NOT_FOUND') {
        throw new Error(`No ${jsFile}.js or ${tsFile}.ts found`);
      }

      throw tsErr;
    }
  }

  if (handler.default) {
    return handler.default;
  }

  return handler;
};

const middleware = async (req: express.Request, res: express.Response) => {
  const fnEvent = new FunctionEvent(req, logger);
  const fnContext = new FunctionContext();

  let output: any;
  try {
    fnEvent.log.trace('Function invoked');
    const handler = await loadHandler();

    output = await handler(fnEvent, fnContext);
    fnEvent.log.trace('Function called successfully');
  } catch (err) {
    fnEvent.log.error({ err }, 'Function errored');
    output = err.toString ? err.toString() : err;
    fnContext.httpStatus = 500;
  }

  const fnResult = isArray(output) || isObject(output) ? JSON.stringify(output) : output;

  fnEvent.log.info(fnContext, 'Function response complete');

  res.status(fnContext.httpStatus).set(fnContext.httpHeaders).send(fnResult);
};

app.post('/*', middleware);
app.get('/*', middleware);
app.patch('/*', middleware);
app.put('/*', middleware);
app.delete('/*', middleware);

const port = Number(process.env.http_port || 3000);

app.listen(port, () => {
  logger.info({ port }, 'OpenFaaS function listening');
});
