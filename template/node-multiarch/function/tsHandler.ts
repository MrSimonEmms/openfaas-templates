/**
 * handler
 *
 * This file is for writing a function in TypeScript. If you want
 * to use JavaScript instead, create a file called "handler.js" and
 * this file will be ignored
 */

/* Node modules */

/* Third-party modules */

/* Files */
import { IFunctionContext, IFunctionEvent } from 'openfaas-node-multiarch';

export default (event: IFunctionEvent, context: IFunctionContext) => {
  const response = {
    status: `Received input: ${JSON.stringify(event.body)}`,
  };

  event.log.info({ input: event.body }, 'Data received');

  return context.status(200).succeed(response);
};
