# OpenFaaS Node CRUD

A template for building CRUD applications using the [NestJS](https://nestjs.com)
and the [NestJS CRUD](https://github.com/nestjsx/crud/wiki/Controllers) library.

## Environment Variables

| Variable | Description | Default |
| --- | --- | --- |
| DB_HOST | Hostname of the database | - |
| DB_LOGGING | Log database queries. This can expose sensitive data so should not be enabled in production| `false` |
| DB_MIGRATIONS_RUN | Automatically run the migrations on application load | `true` |
| DB_NAME | Name of the database | - |
| DB_PASSWORD | Password for the database user | - |
| DB_PORT | Port the database is running on | - |
| DB_SSL_CA | Database SSL certificate authority - only if `DB_USE_SSL` is `true` | - |
| DB_SSL_CERT | Database SSL certificate - only if `DB_USE_SSL` is `true` | - |
| DB_SSL_KEY | Database SSL key - only if `DB_USE_SSL` is `true` | |
| DB_SYNC | Sync database with the entity files on boot - should not be used in production as will destroy any existing data | `false` |
| DB_TYPE | Database type - `mysql`, `mariadb` and `postgres` are currently supported options | `mysql` |
| DB_USE_SSL | Use SSL for the database connection | `false` |
| DB_USERNAME | Username of the database user | - |
| LOG_LEVEL | Log level to display in the terminal | `trace` |
| SERVER_ENABLE_SHUTDOWN_HOOKS | Enable [shutdown hooks](https://docs.nestjs.com/fundamentals/lifecycle-events#application-shutdown) | `true` |
| SERVER_PORT | Port to run on | `3000` |
| SWAGGER_DESCRIPTION | Application description in the Swagger webpage | `<description in package.json>` |
| SWAGGER_ENABLED | Enable the Swagger documentation | `true` |
| SWAGGER_TITLE | Application title in the Swagger webpage | `<name in package.json>` |
| SWAGGER_URL | URL to host Swagger on | `api` |
| SWAGGER_VERSION | Application version in the Swagger webpage | `<version in package.json>` |

## TypeORM

The application is built upon top of [TypeORM](https://docs.nestjs.com/recipes/sql-typeorm).
There is also an npm script configured to simplify interaction with the TypeORM CLI which uses
the environment variables configured.

### TypeORM CLI

```shell
npm run typeorm
```

### Generate a Migration

```shell
npm run typeorm:migration:generate -- -n create-user
```

This will generate a new migration file in `/src/migration`. This will be used to migrate the
database to the latest version at runtime.
