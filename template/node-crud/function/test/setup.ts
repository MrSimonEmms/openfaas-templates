import * as supertest from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import AppModule from '../src/app.module';
import { main as dataIngester } from '../data/init';

let app: INestApplication;

export const destroyApp = async () => {
  if (app) {
    await app.close();

    /* Remove definition to prevent being run again */
    app = undefined;
  }
};

export const request = async (): Promise<
  supertest.SuperTest<supertest.Test>
> => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();

  return supertest(app.getHttpServer());
};

export { supertest };

beforeEach(() => dataIngester(false));

afterEach(() => destroyApp());
