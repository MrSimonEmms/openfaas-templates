import { request, supertest } from './setup';

describe('Controller tests', () => {
  describe('/', () => {
    let app: supertest.Test;

    describe('GET', () => {
      beforeEach(async () => {
        app = (await request()).get('/');
      });

      it('should output list of entities', async () => {
        const { body } = await app.expect(200);

        expect(body).toEqual({
          data: [
            {
              id: 1,
              firstName: 'Test',
              lastName: 'Testington',
              createdAt: expect.any(String),
              updatedAt: expect.any(String),
            },
          ],
          count: expect.any(Number),
          total: expect.any(Number),
          page: expect.any(Number),
          pageCount: expect.any(Number),
        });
      });
    });

    describe('POST', () => {
      beforeEach(async () => {
        app = (await request()).post('/');
      });

      it('should handle invalid input', () =>
        app.send({}).expect(400, {
          statusCode: 400,
          message: [
            'firstName must be shorter than or equal to 200 characters',
            'firstName must be a string',
            'firstName should not be empty',
            'lastName must be shorter than or equal to 200 characters',
            'lastName must be a string',
            'lastName should not be empty',
          ],
          error: 'Bad Request',
        }));

      it('should handle valid input', async () => {
        const input = {
          firstName: 'Test',
          lastName: 'Entry',
        };

        const { body } = await app
          .send({
            firstName: 'Test',
            lastName: 'Entry',
          })
          .expect(201);

        expect(body).toEqual({
          ...input,
          id: expect.any(Number),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
      });
    });
  });

  describe('/:id', () => {
    let app: supertest.SuperTest<supertest.Test>;

    beforeEach(async () => {
      app = await request();
    });

    describe('GET', () => {
      it('should handle not finding an entity', async () =>
        app.get('/0').expect(404));

      it('should find an entity', async () => {
        const { body } = await app.get('/1').expect(200);

        expect(body).toEqual({
          id: 1,
          firstName: 'Test',
          lastName: 'Testington',
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
      });
    });

    describe('PATCH', () => {
      it('should not update an invalid entity', () =>
        app.patch('/0').send({ firstName: 'hello' }).expect(404));

      it('should handle invalid input', () =>
        app
          .patch('/1')
          .send({ firstName: '', lastName: '' })
          .expect(400, {
            statusCode: 400,
            message: [
              'firstName should not be empty',
              'lastName should not be empty',
            ],
            error: 'Bad Request',
          }));

      it('should handle valid input', async () => {
        const input = {
          firstName: 'Newfirstname',
          lastName: 'Newlastname',
        };
        const id = 1;

        const { body } = await app.patch(`/${id}`).send(input).expect(200);

        expect(body).toEqual({
          ...input,
          id,
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
      });
    });

    describe('DELETE', () => {
      it('should not update an invalid entity', () =>
        app.delete('/0').expect(404));

      it('should delete a valid entity', () =>
        app.delete('/1').expect(200, {}));
    });
  });
});
