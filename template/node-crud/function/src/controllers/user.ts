import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';

import Entity from '../entities/user';
import User from '../services/user';

@Crud({
  model: {
    type: Entity,
  },
})
@ApiTags('User')
@Controller('/')
export default class UserController implements CrudController<Entity> {
  constructor(public service: User) {}
}
