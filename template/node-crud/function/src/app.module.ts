import './crud.global'; // Import before importing the modules

import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule, Params, PinoLogger } from 'nestjs-pino';
import { TerminusModule } from '@nestjs/terminus';
import { LoggerOptions } from 'typeorm/logger/LoggerOptions';

import config from './config';
import PinoTypeOrmLogger from './lib/pinoTypeOrmLogger';
import HealthController from './controllers/health';
import UserController from './controllers/user';
import UserEntity from './entities/user';
import UserService from './services/user';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: config,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<Params> =>
        configService.get<Params>('logger'),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule, LoggerModule],
      inject: [ConfigService, PinoLogger],
      useFactory: async (
        configService: ConfigService,
        pinoLogger: PinoLogger,
      ): Promise<TypeOrmModuleOptions> => {
        const db = configService.get<TypeOrmModuleOptions>('db');

        /* Replace the default TypeOrm logger with PinoTypeOrmLogger */
        let logger: 'debug' | PinoTypeOrmLogger = 'debug';
        let logging = false;

        if (configService.get<LoggerOptions>('db.logging', false)) {
          logger = new PinoTypeOrmLogger(pinoLogger);
          logging = true;
        }

        return {
          ...db,
          logging,
          logger,
        };
      },
    }),
    TypeOrmModule.forFeature([UserEntity]),
    TerminusModule,
  ],
  controllers: [HealthController, UserController],
  providers: [UserService],
})
export default class AppModule {}
