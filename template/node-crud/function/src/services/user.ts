import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import User from '../entities/user';

@Injectable()
export default class UserService extends TypeOrmCrudService<User> {
  constructor(@InjectRepository(User) public repo) {
    super(repo);
  }
}
