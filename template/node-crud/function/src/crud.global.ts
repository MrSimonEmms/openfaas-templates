import { CrudConfigService } from '@nestjsx/crud';

CrudConfigService.load({
  routes: {
    exclude: ['createManyBase', 'replaceOneBase'],
  },
  query: {
    alwaysPaginate: true,
    limit: 25,
    maxLimit: 100,
  },
});
