import 'source-map-support/register';

import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import AppModule from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = app.get(ConfigService);
  const logger = await app.resolve(PinoLogger);

  if (config.get<boolean>('swagger.enabled')) {
    const swaggerConfig = config.get('swagger');

    logger.debug({ config: swaggerConfig }, 'Swagger documentation enabled');

    const swagger = new DocumentBuilder()
      .setTitle(swaggerConfig.title)
      .setDescription(swaggerConfig.description)
      .setVersion(swaggerConfig.version)
      .build();
    const document = SwaggerModule.createDocument(app, swagger);
    SwaggerModule.setup(swaggerConfig.url, app, document);
  } else {
    logger.debug('Swagger documentation disabled');
  }

  if (config.get<boolean>('server.enableShutdownHooks')) {
    logger.debug('Enabling shutdown hooks');
    app.enableShutdownHooks();
  } else {
    logger.debug('Shutdown hooks not enabled');
  }

  const port = config.get('server.port');

  logger.debug('Starting HTTP listener');
  await app.listen(port);
  logger.info({ port }, 'Application running');
}

bootstrap().catch((err) => {
  /* Unlikely to get to here but a final catchall */
  console.log(err.stack);
  process.exit(1);
});
