import db from './db';
import logger from './logger';
import server from './server';
import swagger from './swagger';

export default [db, logger, server, swagger];
