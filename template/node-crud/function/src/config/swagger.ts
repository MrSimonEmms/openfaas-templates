import { registerAs } from '@nestjs/config';

export default registerAs('swagger', () => ({
  description:
    process.env.SWAGGER_DESCRIPTION ?? process.env.npm_package_description,
  enabled: process.env.SWAGGER_ENABLED !== 'false',
  title: process.env.SWAGGER_TITLE ?? process.env.npm_package_name,
  url: process.env.SWAGGER_URL ?? 'api',
  version: process.env.SWAGGER_VERSION ?? process.env.npm_package_version,
}));
