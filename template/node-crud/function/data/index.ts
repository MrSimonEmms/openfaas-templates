import { main } from './init';

main()
  .then(() => {
    console.log('Ingestion complete');
    process.exit();
  })
  .catch((err) => {
    console.log('Failed to ingest');
    console.log(err.stack);
    process.exit(1);
  });
