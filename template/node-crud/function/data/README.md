# Data Ingestion

This ingests test data into the development stack. Not for production use.

## Environment Variables

- **DB_TYPE**: Driver to use - defaults to `mysql`

Other envvars are the same as per the main application

## Folder structure

### Data

> **tl;dr** create files in the format `/data/<integer>-<collection>.<js/json>`

This is where the data lives. You can store data either in JSON, JS or TS format - JSON is
static whereas JS/TS can be used if you need to link to other data or calculate things.

The file format **MUST** be `<integer>-<collection>`, eg `001-user.json`. The number
is the position in which it's run (eg, `001` is run before `002`) - this allows
you to link through to data already in a data table (useful for linking foreign
keys in SQL DBs).

The `collection` name is the name of the collection/table - in the above
example, it will be called `user`.

#### JSON

##### Simple

```json
[{
  "firstName": "Test",
  "lastName": "Testington"
}]
```

This can be a simple array of objects. If you use an array of objects, it will
add a `createdAt` and `updatedAt` key with the datetime of when it was created.

##### Complex

If you wish not to have this created/updated keys in, you can use this format:

```json
{
  "meta": {
    "created": false, // Add "created" metadata
    "updated": false // Add "updated" metadata
  },
  // Insert the data here
  "data": [{
    "firstName": "Test",
    "lastName": "Testington"
  }]
}
```

#### JS/TS

You can run any NodeJS script in here. It must return an array of objects
from `module.exports` or `export default`. As with the JSON, you can use either
the simple or the complex format.

### Drivers

This exists to allow for future database types to be added. The driver must implement
the `IDriver` interface.

```typescript
interface IDriver {
  auth(): Promise<void>;
  close(): Promise<void>;
  insertBulk(table: string, data: IData[]): Promise<number>;
  truncate(table: string): Promise<void>;
}
```

#### MySQL

##### Environment Variables

- **DB_HOST**: The DB host, eg `localhost`
- **DB_USERNAME**: The DB username, eg `user`
- **DB_PASSWORD**: The DB password, eg `db-password`
- **DB_NAME**: The DB name, eg `db`
- **DB_PORT**: The DB port, eg `3306`. Defaults to `3306`
