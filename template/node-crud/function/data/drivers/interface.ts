export interface IData {
  [key: string]: any;
}

export interface IDriver {
  auth(): Promise<void>;
  close(): Promise<void>;
  insertBulk(table: string, data: IData[]): Promise<number>;
  truncate(table: string): Promise<void>;
}
