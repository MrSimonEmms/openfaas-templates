import * as mysql from 'mysql2/promise';

import { IData, IDriver } from './interface';

export default class MySQL implements IDriver {
  private db: mysql.Connection;

  readonly opts: mysql.ConnectionOptions;

  constructor() {
    this.opts = {
      host: process.env.DB_HOST,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      port: Number(process.env.DB_PORT ?? 3306),
    };
  }

  async auth() {
    this.db = await mysql.createConnection(this.opts);

    /* Do a dummy query to check connection ok */
    await this.db.query('SELECT 1 + 1');
  }

  async close() {
    await this.db.destroy();
  }

  async insertBulk(table: string, data: IData[]) {
    const columns = Object.keys(data[0])
      .map((item) => `\`${item}\``)
      .join(', ');
    const sql = `INSERT INTO ${table} (${columns}) VALUES ?`;
    const values = data.map((item) => Object.values(item));

    await this.db.query(sql, [values]);

    return values.length;
  }

  async truncate(table: string) {
    await this.db.query(`DELETE from ${table}`);
  }
}
