import * as path from 'path';
// eslint-disable-next-line import/no-extraneous-dependencies
import { sync as glob } from 'glob';
import { IDriver } from './drivers/interface';

// eslint-disable-next-line import/prefer-default-export
export async function getConnection(): Promise<IDriver> {
  const driver = (process.env.DB_TYPE || 'mysql').toLowerCase();

  const { default: Driver } = await import(`./drivers/${driver}`);

  const db = new Driver();

  await db.auth();

  return db;
}

export async function main(log: boolean = false) {
  const connection = await getConnection();

  const src = path.join(__dirname, 'data', '**', '*.{ts,js,json}');

  const files = glob(src, {
    nosort: true,
  }).sort((a, b) => {
    /* Always put link tables to end - assume they've got keys */
    if (a.includes('link_')) {
      return 1;
    }
    if (b.includes('link_')) {
      return -1;
    }
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }

    return 0;
  });

  await files.reduce(
    (thenable, file) =>
      thenable.then(async () => {
        let items = await import(file);

        if (items.default) {
          items = items.default;
        }

        const name = path
          .basename(file, path.extname(file))
          .replace(/^(\d.*-)/, '');

        const { meta = {}, data = items } = items;

        if (!Array.isArray(data)) {
          throw new Error(`Data not an array: ${name}`);
        }

        if (data.length === 0) {
          if (log) {
            console.log(`No data in ${name} - skipping`);
          }
          return;
        }

        /* Clear out any existing data */
        await connection.truncate(name);

        const parsedData = data.map((item) => {
          const now = new Date();
          if (!item.createdAt && meta.created !== false) {
            // eslint-disable-next-line no-param-reassign
            item.createdAt = now;
          }
          if (!item.updatedAt && meta.updated !== false) {
            // eslint-disable-next-line no-param-reassign
            item.updatedAt = now;
          }

          return item;
        });

        try {
          const inserts = await connection.insertBulk(name, parsedData);

          if (log) {
            console.log(`Inserted ${inserts} row(s) to ${name}`);
          }
        } catch (err) {
          if (log) {
            console.log('Error - backing out');
          }

          try {
            await connection.truncate(name);
          } catch (trunErr) {
            if (log) {
              console.log('Erroring backing out');
              console.log(trunErr);
            }
          }

          throw err;
        }
      }),
    Promise.resolve(),
  );

  await connection.close();
}
