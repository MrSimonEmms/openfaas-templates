# OpenFaaS Template

A collection of templates to use with OpenFaaS

## node-crud

A NodeJS template to generate CRUD endpoints with connection to a MongooDB
with Mongoose.

## node-multiarch

A NodeJS template that provides TypeScript support and can provide
multi-arch containers.

This maintains compatibility with the official Node12 template (except
callback support).

Interfaces are published to [npm](https://www.npmjs.com/package/openfaas-node-multiarch)
under the `openfaas-node-multiarch`package.
